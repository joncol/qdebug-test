# qDebug() is Not Working

## Building and Running

    git clone git@bitbucket.org:joncol/qdebug-test.git
    mkdir qdebug-test/_build
    cd qdebug-test/_build
    cmake -DCMAKE_EXPORT_COMPILE_COMMANDS=1 -DCMAKE_BUILD_TYPE=Debug ..
    make
    bin/qdebug_test

## Expected Output

    hello, world
    qDebug() output

## Actual Output

    hello, world
